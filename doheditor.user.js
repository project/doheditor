// ==UserScript==
// @name           DOHeditor
// @author         Valery Lourie
// @version        0.8.0
// @include        http://drupal.org/*
// @include        https://drupal.org/*
// @include        http://drupalmentoring.org/*
// @require        http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
// @grant		   GM_xmlhttpRequest
// @grant		   unsafewindow
// ==/UserScript==
jQuery(function($){
 var Drupal = typeof(window.Drupal)=='undefined' ? unsafeWindow.Drupal : window.Drupal;
 console.log(Drupal);
 var doheditor = {
    updateCheck: function() {
   	  var now = new Date();
   	  // Time of the last update check performed.
   	  var lastUpdateCheck = Drupal.storage.load('dohLastUpdateCheck');
   	  // Time of the last change parsed out of commit log.
   	  var lastUpdate = Drupal.storage.load('dohLastUpdate');

   	  // Do not check for updates if the user just installed Doheditor.
   	  if (lastUpdateCheck == null) {
   	    Drupal.storage.save('dohLastUpdateCheck', now.getTime());
   	    // As we just installed/updated, the last change is also now.
   	    Drupal.storage.save('dohLastUpdate', now.getTime());
   	    return;
   	  }
   	  else {
   	    lastUpdateCheck = new Date(lastUpdateCheck);
   	    lastUpdate = new Date(lastUpdate);
   	  }

   	  // Check whether it is time to check for updates.
	  var interval = 1000 * 60 * 60 * 24 * 14; // 14 days
	  interval = 600; // Temporary! @todo: remove that
	  // Convert to time; JS confuses timezone offset in ISO dates with seconds.
  	  if (lastUpdateCheck.getTime() + interval > now.getTime()) {
   	    return;
   	  }
   	  // Save that a update check was performed.
   	  // Was previously only saved when the user confirmed or when the commit log
   	  // could not be parsed. But if the user does not confirm (cancels), the update
   	  // would run on every page load again.
   	  Drupal.storage.save('dohLastUpdateCheck', now.getTime());

	  var lastChange, doUpdate;
   	  $.ajax({
   	    url: '//drupal.org/node/2000880/commits',
   	    success: function (data) {
   	      lastChange = $('.commit-global:first h3 a:last', data);
   	      $('body').append(lastChange.text());

   	      if (lastChange.length) {
		       lastChange = new Date(lastChange.text());
		       if (lastChange > lastUpdate) {
		          doUpdate = unsafeWindow.confirm('Doheditor got improved! Visit the project page to update?');
		          if (doUpdate) {
		    	     unsafeWindow.open('//drupal.org/project/doheditor', 'doheditor');
		    	     // Update the stored timestamp if the user confirmed.
		    	     Drupal.storage.save('dohLastUpdate', lastChange.getTime());
		    	  }
 		       }
   	      }
   	    }
  	  });
		      
    },
    // Check if this is a core issue page
    coreIssue: function() {
      if ($('#nav-content .links li.core.active').length) {
    	return true;
      }
      else {
    	return false;
      }
    },
    issueId: function() {
      pathname = window.location.pathname;
      var matches = /node\/(\d+)/.exec(pathname);
      return matches[1];
    }
 };

 if (this.domain=='drupalmentoring.org') {
  // fetch this with prepopulate module on doh side.
  if (window.location.search.length > 5) {
	var parts = window.location.search.substr(1).split('&');
	var nid = 0;
	var values;
	for (i=0;i<parts.length;i++) {
	  values=parts[i].split('=');
	  if (values[0]=='nid'){nid=values[1]}
	}
	if (nid>0) $('.change_nid').val(nid);
  }
 }
 // d.o. behavior
 else { 
  //Invoke doheditor update check once.
  doheditor.updateCheck();

  if (doheditor.coreIssue()) {
   var css = document.createElement('style');
   var styles = '#tabs ul.tabs li a.doh-link {font-weight:bold;font-size:bigger;color:#FFF; background-color:#0068C1}';
   css.setAttribute('type', 'text/css');
   if (css.styleSheet) css.styleSheet.cssText = styles;
   else css.appendChild(document.createTextNode(styles));    

   // Inject CSS
   var head = document.getElementsByTagName('head')[0];
   head.appendChild(css);
	
	var nid = doheditor.issueId();
	var dohLink = 'http://drupalmentoring.org/d_o/' + nid;
	console.log('start');
	GM_xmlhttpRequest ( {
	  method:     "GET",
	  url:        dohLink,
	  onerror:     function (response) {
       console.log (response.status, response.responseText.substring (0, 80));
	  },
	  onload:     function (response) {
		 if (response.status==200) {
			$('.tabs.primary').append('<li><a class="doh-link" href="'+dohLink+'">DOH task</a></li>');
      $('.tabs.primary').append('<li><a class="doh-link" href="http://drupalmentoring.org/node/add/task?nid='+nid+'">Create another task</a></li>');
		 }
		 else {
			$('.tabs.primary').append('<li><a class="doh-link" href="http://drupalmentoring.org/node/add/task?nid='+nid+'">No DOH task found!</a></li>');			 
		 }
	  }
	});
  }
 }
});
